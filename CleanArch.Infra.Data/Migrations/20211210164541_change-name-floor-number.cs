﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class changenamefloornumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Order",
                table: "Floors");

            migrationBuilder.AddColumn<int>(
                name: "Number",
                table: "Floors",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "HasQueue",
                table: "Elevators",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Number",
                table: "Floors");

            migrationBuilder.DropColumn(
                name: "HasQueue",
                table: "Elevators");

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Floors",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
