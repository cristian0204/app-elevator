﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Data.Migrations
{
    public partial class AddElevatorCallmodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurrentFloor",
                table: "Elevators",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DoorStatus",
                table: "Elevators",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ElevatorCallSteps",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    UpdatedBy = table.Column<Guid>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    ElevatorId = table.Column<int>(nullable: false),
                    FloorId = table.Column<int>(nullable: false),
                    Priority = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElevatorCallSteps", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ElevatorCallSteps_Elevators_ElevatorId",
                        column: x => x.ElevatorId,
                        principalTable: "Elevators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_ElevatorCallSteps_Floors_FloorId",
                        column: x => x.FloorId,
                        principalTable: "Floors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ElevatorCallSteps_ElevatorId",
                table: "ElevatorCallSteps",
                column: "ElevatorId");

            migrationBuilder.CreateIndex(
                name: "IX_ElevatorCallSteps_FloorId",
                table: "ElevatorCallSteps",
                column: "FloorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ElevatorCallSteps");

            migrationBuilder.DropColumn(
                name: "CurrentFloor",
                table: "Elevators");

            migrationBuilder.DropColumn(
                name: "DoorStatus",
                table: "Elevators");
        }
    }
}
