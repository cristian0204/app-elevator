﻿using CleanArch.Infra.Data.Context;
using Domain.Models.Elevator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Data.Context
{
    public static class ApplicationDBContextSeed
    {
        public static async Task SeedSampleDataAsync(ApplicationDBContext context)
        {
            // Seed, if necessary
            if (!context.Elevators.Any())
            {
                context.Elevators.Add(new Elevator
                {
                    Name = "Main elevator",
                    Status = true,
                    Speed = 1,
                    DoorStatus = 0,
                    CurrentFloor = 1,
                    Floors =
                    {
                        new Floor
                        {
                            Name = "Piso 1",
                            Status = true,
                            ElevatorId = 1,
                            Number = 1
                        },
                        new Floor
                        {
                            Name = "Piso 2",
                            Status = true,
                            ElevatorId = 1,
                            Number = 1
                        }
                    }
                });


                await context.SaveChangesAsync();

            }

        }
    }
}
