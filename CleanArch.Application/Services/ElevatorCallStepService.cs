﻿using Application.Core.Exceptions;
using Application.Interfaces;
using Domain.Interfaces;
using Domain.Models.Elevator;
using EasyCaching.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ElevatorCallStepService : IElevatorCallStepService
    {
        private readonly IUnitOfWork _unitOfWork;
        private const string KEYREDISQUERY = "ElevatorcallStepService";
        private readonly IDistributedCache _distributedCache;


        public ElevatorCallStepService(IUnitOfWork unitOfWork,  IDistributedCache distributedCache)
        {
            _unitOfWork = unitOfWork;
            _distributedCache = distributedCache;
        }
         
        public async Task<ElevatorCallStep> PostElevatorCallStep(ElevatorCallStep elevatorCallStep)
        {
            string key = String.Concat(KEYREDISQUERY, elevatorCallStep.Id);
            var elevatorCallStepResult = await _unitOfWork.elevatorCallStep.Add(elevatorCallStep);
            var serilized = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(elevatorCallStepResult));
            await _distributedCache.SetAsync(key, serilized);

            return elevatorCallStepResult;
        }

        public async Task<IList<ElevatorCallStep>> GetStepsByElevatorId(int ElevatorId)
        {
            string key = String.Concat(KEYREDISQUERY, "_elevator", ElevatorId);
            var cachingSteps = await _distributedCache.GetAsync(key);
            if (cachingSteps != null)
            {
                string serializedSteps = Encoding.UTF8.GetString(cachingSteps);

                return JsonConvert.DeserializeObject<IList<ElevatorCallStep>>(serializedSteps);
            }
            var elevatorCallSteps = await _unitOfWork.elevatorCallStep
                .Get()
                .Include(x => x.Floor)
                .Where(x => x.ElevatorId == ElevatorId)
                .ToListAsync();
            var serilized = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(elevatorCallSteps));
            await _distributedCache.SetAsync(key, serilized);

            return elevatorCallSteps;

        }

        public async Task<List<ElevatorCallStep>> GetStepsByElevatorIdAndFloorId(int ElevatorId, int floorId)
        {
            var elevatorCallSteps = await _unitOfWork.elevatorCallStep
                .Get()
                .Include(x => x.Floor)
                .Where(x => x.ElevatorId == ElevatorId && x.FloorId == floorId)
                .ToListAsync();

            return elevatorCallSteps;
        }

        public async Task RemoveSteps(List<ElevatorCallStep> elevatorCallSteps)
        {
            await _unitOfWork.elevatorCallStep.DeleteRange(elevatorCallSteps);
            foreach (var elevatorCallStep in elevatorCallSteps)
            {
                string key = String.Concat(KEYREDISQUERY, elevatorCallStep.Id);
                await _distributedCache.RemoveAsync(key);
            }
        }




        public async Task<ElevatorCallStep> GetStepById(Guid id)
        {
            string key = String.Concat(KEYREDISQUERY,id);
            var cachingStep = await _distributedCache.GetAsync(key);
            if (cachingStep != null)
            {
                string serialized = Encoding.UTF8.GetString(cachingStep);

                return JsonConvert.DeserializeObject<ElevatorCallStep>(serialized);
            }

            var elevatorCallStep =  await _unitOfWork.elevatorCallStep
                .Get()
                .AsNoTracking()
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();

            var serilized = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(elevatorCallStep));
            await _distributedCache.SetAsync(key, serilized);

            return elevatorCallStep;


        }

        public async Task<ElevatorCallStep> GetNextStep(int id)
        {
            return await _unitOfWork.elevatorCallStep
                .Get()
                .Include(x => x.Floor)
                .Where(x => x.ElevatorId == id)
                .OrderBy(x => x.Priority).ThenBy(x => x.CreatedAt)
                .FirstOrDefaultAsync();
        }

        public async Task Remove(ElevatorCallStep elevatorCallStep)
        {
            await _unitOfWork.elevatorCallStep.Delete(elevatorCallStep);
            string key = String.Concat(KEYREDISQUERY,elevatorCallStep.Id);
            await _distributedCache.RemoveAsync(key);


        }
    }
}
